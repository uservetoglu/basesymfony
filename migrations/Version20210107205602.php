<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210107205602 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE height (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stock (id INT AUTO_INCREMENT NOT NULL, product_id INT DEFAULT NULL, sku VARCHAR(255) NOT NULL, quantity INT NOT NULL, price DOUBLE PRECISION NOT NULL, height DOUBLE PRECISION DEFAULT NULL, weight DOUBLE PRECISION DEFAULT NULL, depth DOUBLE PRECISION DEFAULT NULL, width DOUBLE PRECISION DEFAULT NULL, tax_rate DOUBLE PRECISION DEFAULT NULL, ean VARCHAR(255) DEFAULT NULL, gtin VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_4B3656604584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stock_variant_option (stock_id INT NOT NULL, variant_option_id INT NOT NULL, INDEX IDX_E83F1E2DDCD6110 (stock_id), INDEX IDX_E83F1E2D4438C63C (variant_option_id), PRIMARY KEY(stock_id, variant_option_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE variant_group (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE variant_option (id INT AUTO_INCREMENT NOT NULL, variant_group_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_4FDCA766618498CF (variant_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE variant_option_of_stock (stock_id INT NOT NULL, variant_option_id INT NOT NULL, INDEX IDX_C71F8DADDCD6110 (stock_id), INDEX IDX_C71F8DAD4438C63C (variant_option_id), PRIMARY KEY(stock_id, variant_option_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE stock ADD CONSTRAINT FK_4B3656604584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE stock_variant_option ADD CONSTRAINT FK_E83F1E2DDCD6110 FOREIGN KEY (stock_id) REFERENCES stock (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE stock_variant_option ADD CONSTRAINT FK_E83F1E2D4438C63C FOREIGN KEY (variant_option_id) REFERENCES variant_option (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE variant_option ADD CONSTRAINT FK_4FDCA766618498CF FOREIGN KEY (variant_group_id) REFERENCES variant_group (id)');
        $this->addSql('ALTER TABLE variant_option_of_stock ADD CONSTRAINT FK_C71F8DADDCD6110 FOREIGN KEY (stock_id) REFERENCES variant_option (id)');
        $this->addSql('ALTER TABLE variant_option_of_stock ADD CONSTRAINT FK_C71F8DAD4438C63C FOREIGN KEY (variant_option_id) REFERENCES stock (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE stock DROP FOREIGN KEY FK_4B3656604584665A');
        $this->addSql('ALTER TABLE stock_variant_option DROP FOREIGN KEY FK_E83F1E2DDCD6110');
        $this->addSql('ALTER TABLE variant_option_of_stock DROP FOREIGN KEY FK_C71F8DAD4438C63C');
        $this->addSql('ALTER TABLE variant_option DROP FOREIGN KEY FK_4FDCA766618498CF');
        $this->addSql('ALTER TABLE stock_variant_option DROP FOREIGN KEY FK_E83F1E2D4438C63C');
        $this->addSql('ALTER TABLE variant_option_of_stock DROP FOREIGN KEY FK_C71F8DADDCD6110');
        $this->addSql('DROP TABLE height');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE stock');
        $this->addSql('DROP TABLE stock_variant_option');
        $this->addSql('DROP TABLE variant_group');
        $this->addSql('DROP TABLE variant_option');
        $this->addSql('DROP TABLE variant_option_of_stock');
    }
}
