<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210205085134 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE popular_category_link (id INT AUTO_INCREMENT NOT NULL, page_url VARCHAR(255) DEFAULT NULL, categories LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', status TINYINT(1) DEFAULT \'1\' NOT NULL, slug VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_general_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE category ADD is_use_rule_set TINYINT(1) DEFAULT \'1\' NOT NULL');
        $this->addSql('ALTER TABLE stock ADD status TINYINT(1) DEFAULT \'1\' NOT NULL, ADD has_discount TINYINT(1) DEFAULT \'0\' NOT NULL, ADD discount_price DOUBLE PRECISION NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE popular_category_link');
        $this->addSql('ALTER TABLE brand DROP is_use_rule_set');
        $this->addSql('ALTER TABLE stock DROP status, DROP has_discount, DROP discount_price');
    }
}
