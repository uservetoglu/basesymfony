<?php


namespace App\Util\Cache;


interface CacheInterface
{
    public function set($key, $cache, $timeout = 3600);
    public function get($key);
}
