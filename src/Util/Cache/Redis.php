<?php


namespace App\Util\Cache;

use Predis\Client;
use RedisCluster;
use Symfony\Component\Cache\Adapter\RedisAdapter;
use Symfony\Component\DependencyInjection\ContainerInterface;


class Redis implements CacheInterface
{
    /**
     * @var Client|\Redis|RedisCluster
     */
    private $cache;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $redisDsn = $container->getParameter('redis_dsn');
        $this->cache = RedisAdapter::createConnection($redisDsn);
    }

    /**
     * @param $key
     * @param $cache
     * @param int $timeout
     */
    public function set($key, $cache, $timeout = 3600): void
    {
        $this->cache->set($key, serialize($cache));
        $this->cache->expire($key, $timeout);
    }

    /**
     * @param $key
     *
     * @return object|array|null
     */
    public function get($key)
    {
        return $this->cache->get($key) ? unserialize($this->cache->get($key)) : null;
    }
}
