<?php

namespace App\Response;

use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class AbstractResponse
{
    /**
     * @var int
     */
    protected $code = 200;

    /**
     * @var array
     */
    protected $data = [];

    /**
     * @var array
     */
    protected $filter = [];

    /**
     * @var string
     */
    protected $message = null;

    /**
     * @var array
     */
    protected $errors = null;

    /**
     * @var null
     */
    protected $pagination = null;

    /**
     * @var array
     */
    protected $contextGroups = array();

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @return mixed
     */
    public abstract function response();

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param array|object $data
     */
    public function setData($data): void
    {
        $resultSet = $data;
        if ($data instanceof RepositoryResponse) {

            $resultSet = $data->getResultSet();

            $filter = $this->filter;

            $filter['recordCount'] = $data instanceof RepositoryResponse ? $data->getTotalRecords() : $filter['recordCount'];

            $pagination = $filter['recordCount'] == 0 || !empty($this->errors) ? null :
                (new ApiPagination())
                    ->paginate($filter['limit'], $filter['recordCount'], $filter['offset'])
                    ->getConvertObject();

            $this->setPagination($pagination);
        }

        $this->data = $resultSet;
    }

    /**
     * @param array $filter
     */
    public function setFilter(array $filter): void
    {
        $this->filter = $filter;
    }

    /**
     * @param array $errors
     */
    public function setErrors(array $errors): void
    {
        $this->errors = $errors;
    }

    /**
     * @param null $pagination
     */
    public function setPagination($pagination): void
    {
        $this->pagination = $pagination;
    }

    /**
     * @param array $contextGroups
     */
    public function setGroup(array $contextGroups): void
    {
        $this->contextGroups = $contextGroups;
    }
}
