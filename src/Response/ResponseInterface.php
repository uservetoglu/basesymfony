<?php

namespace App\Response;

interface ResponseInterface
{
    /**
     * @return mixed
     */
    public function response();

    /**
     * @param array $errors
     */
    public function setErrors(array $errors);

    /**
     * @param object|array $data
     */
    public function setData($data);

    /**
     * @param array $filter
     */
    public function setFilter(array $filter);

    /**
     * @param null $pagination
     */
    public function setPagination($pagination);

    /**
     * @param array $contextGroups
     */
    public function setGroup(array $contextGroups);
}
