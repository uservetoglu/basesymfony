<?php

namespace App\Response;

class RepositoryResponse
{
    /**
     * @var
     */
    private $resultSet;

    /**
     * @var int
     */
    private $totalRecords;

    public function __construct($resultSet, int $totalRecords = 1)
    {
        $this->setResultSet($resultSet);
        $this->setTotalRecords($totalRecords);
    }

    /**
     * @return array
     */
    public function getResultSet()
    {
        return $this->resultSet;
    }

    /**
     * @param array $resultSet
     */
    public function setResultSet($resultSet): void
    {
        $this->resultSet = $resultSet;
    }

    /**
     * @return int
     */
    public function getTotalRecords()
    {
        return $this->totalRecords;
    }

    /**
     * @param int $totalRecords
     *
     * @return $this
     */
    public function setTotalRecords(int $totalRecords)
    {
        $this->totalRecords = $totalRecords;
        return $this;
    }
}
