<?php

namespace App\Response;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse as JSP;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;

class JsonResponse extends AbstractResponse implements ResponseInterface
{
    /**
     * @return array|mixed
     */
    function response()
    {
        $data = $this->createResponseModel();

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setStatusCode($this->code);

        if ($this->container->has('serializer')) {
            $response->setContent($this->setSerialize($data));

            return $response;
        }

        $response->setContent($data);

        return $response;
    }

    /**
     * @return array
     */
    private function createResponseModel()
    {
        $data = [];

         if (!is_null($this->message) && $this->message !== '') {
             $data = array('message' => $this->message);
         }

         if (!is_null($this->errors) && !empty($this->errors)) {
             $data['errors'] = $this->errors;
         }

         if ((is_array($this->data) && !empty($this->data))) {
             $data['result'] = $this->data;
         }

         if ($this->pagination) {
             $data['pagination'] = $this->pagination;
         }

        return $data;
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    private function setSerialize($data)
    {
        $context = ['json_encode_options' => JSP::DEFAULT_ENCODING_OPTIONS, AbstractObjectNormalizer::SKIP_NULL_VALUES => true];
        $json = $this->container->get('serializer')->serialize($data, 'json', array_merge($context, $this->contextGroups));

        return $json;
    }
}
