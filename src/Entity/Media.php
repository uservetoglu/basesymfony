<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Traits\SoftDeletableTrait;
use App\Traits\EntityDateInformationTrait;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MediaItemRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Media
{
    use EntityDateInformationTrait;
    use SoftDeletableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", options={"comment":"Car upload file, car file"})
     */
    private $name;

    /**
     *
     * @ORM\OneToMany(targetEntity="App\Entity\MediaItem", mappedBy="media")
     * @ORM\JoinColumn(nullable=true)
     */
    private $mediaItems;

    /**
     * @var Media
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Product", mappedBy="media")
     */
    private $product;
}
