<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Traits\SoftDeletableTrait;
use App\Repository\VariantGroupRepository;
use App\Traits\EntityDateInformationTrait;

/**
 * @ORM\Entity(repositoryClass=VariantGroupRepository::class)
 */
class VariantGroup
{
    use EntityDateInformationTrait;
    use SoftDeletableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var VariantOption
     *
     * @ORM\OneToMany(targetEntity="App\Entity\VariantOption", mappedBy="variantGroup")
     */
    private $variantOptions;




    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
