<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Traits\SoftDeletableTrait;
use App\Traits\SeoInformationTrait;
use App\Repository\ProductRepository;
use App\Traits\EntityDateInformationTrait;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product
{
    use SoftDeletableTrait;
    use SeoInformationTrait;
    use EntityDateInformationTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="boolean", options={"default":true})
     */
    private $status;

    /**
     * @ORM\Column(type="integer", options={"default":0})
     */
    private $sort;

    /**
     * @var Brand
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Brand", inversedBy="products")
     */
    private $brand;

    /**
     * @var Stock
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Stock", mappedBy="product")
     */
    private $stocks;

    /**
     * @var Media
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Media", inversedBy="product")
     */
    private $media;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}
