<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\BrandRepository;

/**
 * @ORM\Entity(repositoryClass=BrandRepository::class)
 */
class PopularCategoryLink
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="page_url", type="string", nullable=true)
     */
    protected $pageUrl;

    /**
     * @var array
     *
     * @ORM\Column(name="categories", type="array", nullable=true)
     */
    protected $categories;

    /**
     * @var int
     *
     * @ORM\Column(type="boolean", options={"default" :true})
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", nullable=true)
     */
    protected $slug;

}
