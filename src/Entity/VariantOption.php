<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Traits\SoftDeletableTrait;
use App\Traits\EntityDateInformationTrait;
use App\Repository\VariantOptionRepository;

/**
 * @ORM\Entity(repositoryClass=VariantOptionRepository::class)
 */
class VariantOption
{
    use EntityDateInformationTrait;
    use SoftDeletableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\VariantGroup", inversedBy="variantOptions")
     * @ORM\JoinColumn(nullable=true)
     */
    private $variantGroup;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Stock", inversedBy="variantOptions"))
     * @ORM\JoinTable(
     *     name="variant_option_of_stock",
     *     joinColumns={@ORM\JoinColumn(name="stock_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="variant_option_id", referencedColumnName="id")}
     * )
     */
    private $stocks;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }
}
