<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Traits\SoftDeletableTrait;
use App\Traits\EntityDateInformationTrait;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MediaItemRepository")
 * @ORM\HasLifecycleCallbacks
 */
class MediaItem
{
    use SoftDeletableTrait;
    use EntityDateInformationTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", options={"default" :true})
     */
    private $status;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $fileName;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $filePath;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $thumbPath;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Media", inversedBy="mediaItems")
     * @ORM\JoinColumn(nullable=true)
     */
    private $media;
}
