<?php

namespace App\Model;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema()
 */
class CategoryTree
{
    CONST CACHE_CATEGORY_TREE = "S_Category_Tree";

    /**
     * @OA\Property(type="integer", nullable=false)
     */
    protected $id;

    /**
     * @OA\Property(type="string", nullable=false)
     */
    protected $title;

    /**
     * @OA\Property(type="string", nullable=false)
     */
    protected $description;

    /**
     * @OA\Property(type="string", nullable=false)
     */
    protected $slug;

    /**
     * @OA\Property(type="string", nullable=false)
     */
    protected $children;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return array
     */
    public function getChildren(): array
    {
        return $this->children;
    }

    /**
     * @param array $children
     */
    public function setChildren(array $children): void
    {
        $this->children = $children;
    }
}
