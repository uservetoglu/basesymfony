<?php

namespace App\Repository;

use App\Entity\VariantGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method VariantGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method VariantGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method VariantGroup[]    findAll()
 * @method VariantGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VariantGroupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VariantGroup::class);
    }

    // /**
    //  * @return VariantGroup[] Returns an array of VariantGroup objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?VariantGroup
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
