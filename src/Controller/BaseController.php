<?php

namespace App\Controller;

use App\Response\JsonResponse;
use App\Response\ResponseInterface;
use App\Util\Validation\ValidationUtil;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BaseController extends AbstractController
{
    /**
     * @var ValidationUtil
     */
    private $validationUtil;

    /**
     * @param ValidationUtil $validationUtil
     */
    public function __construct(ValidationUtil $validationUtil)
    {
        $this->validationUtil = $validationUtil;
    }

    /**
     * @param ResponseInterface $jsonResponse
     *
     * @return mixed
     */
    public function jsonResponse(JsonResponse $jsonResponse)
    {
        return $jsonResponse->response();
    }

    /**
     * @param ResponseInterface $jsonResponse
     */
    public function viewResponse(JsonResponse $jsonResponse)
    {
     /**
      * this method render view
      */
    }

    /**
     * @param $request
     * @param null $recordCount
     *
     * @return array
     */
    public function queryToFilter($request, $recordCount = null)
    {
        $filter['limit'] = 10;

        if ($recordCount) {
            $filter['recordCount'] = $recordCount;
        }

        foreach ($request->query->all() as $key => $value) {
            $filter[$key] = $value;
        }

        $filter['offset'] = isset($filter['offset']) ? $filter['offset'] : 0;

        $filter['limit'] = intval($filter['limit']) == 0 ? null : $filter['limit'];

        return $filter;
    }
}