<?php

namespace App\Controller;

use App\Response\JsonResponse;
use OpenApi\Annotations as OA;
use App\Service\CategoryService;
use App\Response\ResponseInterface;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends BaseController
{
    /**
     * @Route(path="/categoryTree")
     *
     * @OA\Get(
     *     path="/categoryTree",
     *     @OA\Response(
     *          response="200",
     *          description="CategoryTree",
     *          @OA\JsonContent(type="string", description="categoryTree")
     *     )
     * )
     *
     * @param CategoryService $categoryService
     * @param ResponseInterface $response
     *
     * @return JsonResponse
     */
    public function categoryTree(CategoryService $categoryService, ResponseInterface $response)
    {
        $tree = $categoryService->getCategoryTree();

        $response->setData($tree);
        return $this->jsonResponse($response);
    }
}
