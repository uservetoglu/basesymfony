<?php

namespace App\Command;

use App\Service\Ticimax\ProductService;
use App\Service\Ticimax\Model\CategoryModel;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SyncCategoryCommand extends Command
{
    protected static $defaultName = 'app:sync:categories';

    /**
     * @var ProductService
     */
    private $service;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ProductService $service
     * @param ContainerInterface $container
     */
    public function __construct(ProductService $service, ContainerInterface $container)
    {
        parent::__construct();

        $this->service = $service;
        $this->container = $container;
    }


    public function execute(InputInterface $input, OutputInterface $output)
    {
        $model = new CategoryModel();
        $model->setKategoriId(0);
        $model->setUyeKodu($this->container->getParameter('ticimax')['permision_code']);

        $result = $this->service->getCategory($model);

        $result = $result->SelectKategoriResult->Kategori;

        uasort($result, function($a, $b) {
            if ($a == $b) {
                return 0;
            }

            return ($a < $b) ? -1 : 1;
        });

        $this->service->syncCategoriesOnTicimax(json_decode(json_encode($result), true));
        $this->service->syncCategoriesOnTicimax(json_decode(json_encode($result), true));
        $output->write('Success');

        return Command::SUCCESS;
    }
}
