<?php

namespace App\Service\Ticimax\Model;

class UserModel
{
    /** @var string */
    public $Mail;

    /** @var string */
    public $Sifre;

    /** @var bool */
    public $Admin = false;

    /** @var string */
    public $Otp = '';

    /**
     * @param string $Mail
     */
    public function setMail(string $Mail): void
    {
        $this->Mail = $Mail;
    }

    /**
     * @param string $Sifre
     */
    public function setSifre(string $Sifre): void
    {
        $this->Sifre = $Sifre;
    }

    /**
     * @param bool $Admin
     */
    public function setAdmin(bool $Admin): void
    {
        $this->Admin = $Admin;
    }

    /**
     * @param string $Otp
     */
    public function setOtp(string $Otp): void
    {
        $this->Otp = $Otp;
    }
}
