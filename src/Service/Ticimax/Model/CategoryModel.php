<?php

namespace App\Service\Ticimax\Model;

class CategoryModel
{
    /** @var string */
    public $KategoriId;

    /** @var string */
    public $UyeKodu;

    /**
     * @param string $KategoriId
     */
    public function setKategoriId(string $KategoriId): void
    {
        $this->KategoriId = $KategoriId;
    }

    /**
     * @param string $uyeKodu
     */
    public function setUyeKodu(string $uyeKodu): void
    {
        $this->UyeKodu = $uyeKodu;
    }
}
