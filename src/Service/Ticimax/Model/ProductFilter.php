<?php

namespace App\Service\Ticimax\Model;

class ProductFilter
{
    /**
     * @var int
     */
    public $Aktif = -1;

    /**
     * @var int
     */
    public $Firsat = -1;

    /**
     * @var int
     */
    public $Indirimli = -1;

    /**
     * @var int
     */
    public $Vitrin = -1;

    /**
     * @var int
     */
    public $KategoriID = 0;

    /**
     * @var int
     */
    public $MarkaID = 0;

    /**
     * @var int
     */
    public $UrunKartiID = 0;

    /**
     * @var int|null
     */
    public $ToplamStokAdediBas = null;

    /**
     * @var null|int
     */
    public $ToplamStokAdediSon = null;

    /**
     * @var int
     */
    public $TedarikciID = 0;

    /**
     * @param int $Aktif
     */
    public function setAktif(int $Aktif): void
    {
        $this->Aktif = $Aktif;
    }

    /**
     * @param int $Firsat
     */
    public function setFirsat(int $Firsat): void
    {
        $this->Firsat = $Firsat;
    }

    /**
     * @param int $Indirimli
     */
    public function setIndirimli(int $Indirimli): void
    {
        $this->Indirimli = $Indirimli;
    }

    /**
     * @param int $Vitrin
     */
    public function setVitrin(int $Vitrin): void
    {
        $this->Vitrin = $Vitrin;
    }

    /**
     * @param int $KategoriID
     */
    public function setKategoriID(int $KategoriID): void
    {
        $this->KategoriID = $KategoriID;
    }

    /**
     * @param int $MarkaID
     */
    public function setMarkaID(int $MarkaID): void
    {
        $this->MarkaID = $MarkaID;
    }

    /**
     * @param int $UrunKartiID
     */
    public function setUrunKartiID(int $UrunKartiID): void
    {
        $this->UrunKartiID = $UrunKartiID;
    }

    /**
     * @param int|null $ToplamStokAdediBas
     */
    public function setToplamStokAdediBas(?int $ToplamStokAdediBas): void
    {
        $this->ToplamStokAdediBas = $ToplamStokAdediBas;
    }

    /**
     * @param int|null $ToplamStokAdediSon
     */
    public function setToplamStokAdediSon(?int $ToplamStokAdediSon): void
    {
        $this->ToplamStokAdediSon = $ToplamStokAdediSon;
    }

    /**
     * @param int $TedarikciID
     */
    public function setTedarikciID(int $TedarikciID): void
    {
        $this->TedarikciID = $TedarikciID;
    }
}
