<?php

namespace App\Service\Ticimax\Model;

class ProductModel
{
    /**
     * @var string
     */
    public $UyeKodu;

    /**
     * @var ProductFilter
     */
    public $f;

    /**
     * @var Pagination
     */
    public $s;

    /**
     * @param string $UyeKodu
     */
    public function setUyeKodu(string $UyeKodu): void
    {
        $this->UyeKodu = $UyeKodu;
    }

    /**
     * @param ProductFilter $f
     */
    public function setF(ProductFilter $f): void
    {
        $this->f = $f;
    }

    /**
     * @param Pagination $s
     */
    public function setS(Pagination $s): void
    {
        $this->s = $s;
    }
}
