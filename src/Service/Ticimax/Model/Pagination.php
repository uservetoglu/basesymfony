<?php

namespace App\Service\Ticimax\Model;

class Pagination
{
    /**
     * @var int
     */
    public $BaslangicIndex = 1;

    /**
     * @var int
     */
    public $KayitSayisi = 1;

    /**
     * @var int
     */
    public $SiralamaDegeri = 'id';

    /**
     * @var int
     */
    public $SiralamaYonu = 'DESC';

    /**
     * @param int $BaslangicIndex
     */
    public function setBaslangicIndex(int $BaslangicIndex): void
    {
        $this->BaslangicIndex = $BaslangicIndex;
    }

    /**
     * @param int $KayitSayisi
     */
    public function setKayitSayisi(int $KayitSayisi): void
    {
        $this->KayitSayisi = $KayitSayisi;
    }

    /**
     * @param int $SiralamaDegeri
     */
    public function setSiralamaDeger(int $SiralamaDegeri): void
    {
        $this->SiralamaDegeri = $SiralamaDegeri;
    }

    /**
     * @param int $SiralamaYonu
     */
    public function setSiralamaYonu(int $SiralamaYonu): void
    {
        $this->SiralamaYonu = $SiralamaYonu;
    }
}
