<?php

namespace App\Service\Ticimax;

use App\Service\Ticimax\Model\UserModel;

class UserService
{
    /**
     * @return \SoapClient
     *
     * @throws \SoapFault
     *
     * @throws \Exception
     */
    public static function userClient()
    {
        try {
            return new \SoapClient(
                'https://www.grimelange.com.tr/servis/UyeServis.svc?wsdls',
                array(
                    'trace' => true,
                )
            );
        } catch (\Throwable $exception) {
            throw new \Exception(sprintf('[App\Service\Ticimax][UserService][userClient], %s', $exception->getMessage()));
        }
    }

    /**
     * @param UserModel $user
     *
     * @return object|null
     *
     * @throws \Exception
     */
    public function login(UserModel $user): ?object
    {
        try {
            return self::userClient()->GirisYap(array('ug' => json_decode(json_encode($user), true)));
        } catch (\Throwable $exception) {
            throw new \Exception(sprintf('[App\Service\Ticimax][UserService][login], %s', $exception->getMessage()));
        }

        return null;
    }
}
