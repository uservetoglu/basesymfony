<?php

namespace App\Service\Ticimax;

use App\Entity\Category;
use App\Service\AbstractService;
use App\Service\Ticimax\Model\CategoryModel;
use App\Service\Ticimax\Model\ProductModel;

class ProductService extends AbstractService
{
    /**
     * @return \SoapClient
     *
     * @throws \SoapFault
     */
    public static function productClient()
    {
        return new \SoapClient(
            'https://www.grimelange.com.tr/servis/UrunServis.svc?wsdl',
            array(
                'trace' => true,
            )
        );
    }

    /**
     * @param CategoryModel $model
     *
     * @return object|null
     *
     * @throws \Exception
     */
    public function getCategory(CategoryModel $model)
    {
        try {
            return self::productClient()->SelectKategori(json_decode(json_encode($model), true));
        } catch (\Throwable $exception) {
            throw new \Exception(sprintf('[App\Service\Ticimax][ProductService][getCategory], %s', $exception->getMessage()));
        }

        return null;
    }


    /**
     * @param CategoryModel $model
     *
     * @return object|null
     *
     * @throws \Exception
     */
    public function getProduct(ProductModel $model)
    {
        try {
            $time_start = microtime(true);

            $request = self::productClient()->SelectUrun($model);

            file_put_contents('test.txt', json_encode($request->SelectUrunResult) );
            die;
            return $request;
        } catch (\Throwable $exception) {
            throw new \Exception(sprintf('[App\Service\Ticimax][ProductService][getProduct], %s', $exception->getMessage()));
        }

        return null;
    }

    /**
     * @param array $result
     */
    public function syncCategoriesOnTicimax(array $result): void
    {
        $repo = $this->em->getRepository(Category::class);

        foreach ($result as $category) {
            if (is_null($categoryEntity = $repo->find($category['ID']))) {
                $categoryEntity = new Category();
            }

            $categoryEntity->setId($category['ID']);
            $categoryEntity->setTitle($category['Tanim']);

            /** @var Category $parent */
            if (!is_null($parent = $repo->find($category['PID']))) {
                $categoryEntity->setParent($parent);
            }

            $categoryEntity->setSort($category['Sira']);
            $categoryEntity->setStatus($category['Aktif']);
            $categoryEntity->setDescription($category['Icerik']);
            $categoryEntity->setMetaTitle($category['SeoSayfaBaslik']);
            $categoryEntity->setMetaDescription($category['SeoSayfaAciklama']);
            $categoryEntity->setMetaKeywords($category['SeoAnahtarKelime']);
            $categoryEntity->setSlug(ltrim($category['Url'], '/'));

            $this->em->getManager()->persist($categoryEntity);
        }

        $this->em->getManager()->flush();
    }
}
