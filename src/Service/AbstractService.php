<?php


namespace App\Service;

use App\Util\Cache\Redis;
use App\Util\Cache\CacheInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class AbstractService
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var object
     */
    protected $em;

    /**
     * @var CacheInterface
     */
    protected $cache;

    /**
     * AbstractService constructor.
     * @param ContainerInterface $container
     * @param Redis $cache
     */
    public function __construct(ContainerInterface $container, Redis $cache)
    {
        $this->container = $container;
        $this->em = $container->get('doctrine');
        $this->cache = $cache;
    }

    /**
     * @param $entity
     */
    protected function persist($entity)
    {
        $this->em->getManager()->persist($entity);
        $this->em->getManager()->flush();
    }

    /**
     * @param $json
     * @param $key
     * @param $search
     *
     * @return bool
     */
    protected function jsonSearch($json, $key, $search)
    {
        foreach ($json as $item) {
            if ($item->{$key} === $search) {
                return true;
            }
        }
        return false;
    }

    protected function saveJson($path, $data)
    {
        $fp = fopen($path, 'w');
        fwrite($fp, $this->container->get('serializer')->serialize($data, 'json'));
        fclose($fp);
    }
}
