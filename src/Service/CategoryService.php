<?php


namespace App\Service;


use App\Entity\Category;
use App\Model\CategoryTree;

class CategoryService extends AbstractService
{
    /**
     * @param $categoryId
     * @param bool|null $status
     *
     * @param false|null $noCache
     *
     * @return Category|object
     */
    public function getCategoryById($categoryId, $status = null, $noCache = false)
    {
        $cacheId = \App\Model\Category::CACHE_CATEGORY_BY_ID;

        $category = $this->cache->get($cacheId);
        if (!$category || $noCache) {

            $condition = ['id' => $categoryId];

            if (!is_null($status)) {
                $condition['status'] = $status;
            }

            $category = $this->em->getRepository(Category::class)->findOneBy($condition);
            $this->cache->set($cacheId, $category);
        }

        return $category;
    }

    /**
     * @param false $noCache
     *
     * @return array|object|null
     */
    public function getCategoryTree($noCache = false)
    {
        $cacheId = CategoryTree::CACHE_CATEGORY_TREE;

        $categoryTree = $this->cache->get($cacheId);

        if (!$categoryTree || $noCache) {
            $categories = $this->em
                ->getRepository(Category::class)
                ->findBy(['parent'=> null, 'status' => true]);

            $categoryTree = $this->prepareCategoryTree($categories);

            $this->cache->set($cacheId, $categoryTree);
        }

        return $categoryTree;
    }

    /**
     * @param $categories
     *
     * @return array
     */
    public function prepareCategoryTree($categories)
    {
        $allCategory = [];

        /**  @var  Category $category */
        foreach ($categories as $key => $category) {

            $categoryModel = new CategoryTree();
            $categoryModel->setId($category->getId());
            $categoryModel->setTitle($category->getTitle());
            $categoryModel->setDescription($category->getDescription());
            $categoryModel->setSlug($category->getSlug());

            $child = $this->em->getRepository(Category::class)->findBy(['parent' => $category->getId()]);


            if (!is_null($child)) {
                $categoryModel->setChildren($this->prepareCategoryTree($child));
            }

            $allCategory[] = $categoryModel;
        }

        return $allCategory;
    }

}
