<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 1003; $i < 5000; $i++) {
            $user = new User();
            $user->setUsername('admin'.$i);
            $user->setEmail('admin'.$i);
            $user->setPassword('admin'.$i);
            $manager->persist($user);
        }
        $manager->flush();
    }
}
